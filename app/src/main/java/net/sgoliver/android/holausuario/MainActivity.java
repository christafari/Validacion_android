package net.sgoliver.android.holausuario;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtPassword;
    private Button btnAceptar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        btnAceptar = (Button)findViewById(R.id.BtnAceptar);


        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = ("christian");
                String password = ("12345");
                txtNombre = (EditText)findViewById(R.id.TxtNombre);
                txtPassword = (EditText)findViewById(R.id.TxtPassword);
                if (txtNombre.getText().toString().equals(usuario) &&txtPassword.getText().toString().equals(password)){
                    Intent intent = new Intent(MainActivity.this, SaludoActivity.class);
                    Bundle b = new Bundle();
                    b.putString("NOMBRE", txtNombre.getText().toString());
                    intent.putExtras(b);
                    b.putString("PASSWORD", txtPassword.getText().toString());
                    intent.putExtras(b);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(MainActivity.this, "Incorrecto",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
